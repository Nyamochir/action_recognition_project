
import cv2
from threading import Thread, Lock

class Rstp_stream(Thread):

    def __init__(self, ip):
        Thread.__init__(self)
        self.vcap = cv2.VideoCapture()
        self.vcap.open("rtsp://admin:ypol123!@"+ip+":554/cam/realmonitor?channel=1&subtype=0")
        self.cur_frame = None
        self.lock = Lock()
        self.daemon = True

    def run(self):

        while True:

            ret, frame = self.vcap.read()
            if ret :
                self.lock.acquire()
                self.cur_frame = frame
                self.lock.release()


    def get_frame(self):

        self.lock.acquire()
        frame = self.cur_frame
        self.lock.release()
        return frame