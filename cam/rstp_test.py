import cv2
from Rstp_steam import Rstp_stream

cv2.namedWindow('image',cv2.WINDOW_NORMAL)
cam_1 = Rstp_stream('192.168.0.109')

record_file_name = './recorded.mp4'

cam_1.start()
# Define the codec and create VideoWriter object
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter(record_file_name, fourcc, 20.0, (1920,  1080))

while(True):
    #ret, frame = vcap.read()

    frame = cam_1.get_frame()

    if frame is not None:

        out.write(frame)

        cv2.imshow('image', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
out.release()
cv2.destroyAllWindows()